package observer.notification;

import observer.subscribe.Subscriber;

public interface Notifier {

	void subscribe(Subscriber subscriber);

	void unsubscribe(Subscriber subscriber);

	void notifySubscribers(String word);

}
