package observer.notification.impl;

import java.util.ArrayList;
import java.util.List;

import observer.notification.Notifier;
import observer.subscribe.Subscriber;

public class WordNotifier implements Notifier {

	private List<Subscriber> subscribers;

	public WordNotifier() {
		subscribers = new ArrayList<Subscriber>();
	}

	@Override
	public void subscribe(Subscriber subscriber) {
		subscribers.add(subscriber);
	}

	@Override
	public void unsubscribe(Subscriber subscriber) {
		subscribers.remove(subscriber);
	}

	@Override
	public void notifySubscribers(String word) {
		for (Subscriber subscriber : subscribers) {
			subscriber.processWord(word);
		}
	}
}
