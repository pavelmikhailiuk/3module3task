package observer.main;

import observer.notification.Notifier;
import observer.notification.impl.WordNotifier;
import observer.reader.WordReader;
import observer.reader.impl.WordReaderImpl;
import observer.subscribe.impl.LongestWordKeeper;
import observer.subscribe.impl.NumberCounter;
import observer.subscribe.impl.WordCounter;
import observer.subscribe.impl.WordReverser;

public class Runner {
	public static void main(String[] args) {
		Notifier notifier = new WordNotifier();
		notifier.subscribe(new LongestWordKeeper());
		notifier.subscribe(new NumberCounter());
		notifier.subscribe(new WordCounter());
		notifier.subscribe(new WordReverser());
		WordReader reader = new WordReaderImpl("text.txt", notifier);
		reader.read();
	}
}
