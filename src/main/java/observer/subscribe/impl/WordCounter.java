package observer.subscribe.impl;

import observer.subscribe.Subscriber;

public class WordCounter implements Subscriber {

	private int count;

	public void processWord(String word) {
		count = word != null ? count++ : count;
	}

	public int getCount() {
		return count;
	}
}
