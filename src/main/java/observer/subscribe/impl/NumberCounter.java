package observer.subscribe.impl;

import observer.subscribe.Subscriber;

public class NumberCounter implements Subscriber {

	private int count;

	public void processWord(String word) {
		try {
			if (word != null) {
				Integer.parseInt(word);
				count++;
			}
		} catch (NumberFormatException e) {
		}
	}

	public int getCount() {
		return count;
	}
}
