package observer.subscribe.impl;

import observer.subscribe.Subscriber;

public class WordReverser implements Subscriber {

	private String reversed;

	public void processWord(String word) {
		reversed = word != null ? new StringBuilder(word).reverse().toString() : null;
	}

	public String getReversed() {
		return reversed;
	}
}
