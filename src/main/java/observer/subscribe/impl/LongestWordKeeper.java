package observer.subscribe.impl;

import observer.subscribe.Subscriber;

public class LongestWordKeeper implements Subscriber {

	private String longestWord = "";

	@Override
	public void processWord(String word) {
		longestWord = word != null && (word.length() - longestWord.length()) > 0 ? word : longestWord;
	}

	public String getLongestWord() {
		return longestWord;
	}

}
