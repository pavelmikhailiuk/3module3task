package observer.subscribe;

public interface Subscriber {

	void processWord(String word);

}
