package observer.reader.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import observer.notification.Notifier;
import observer.reader.WordReader;

public class WordReaderImpl implements WordReader {

	private String fileName;

	private Notifier notifier;

	public WordReaderImpl(String fileName, Notifier notifier) {
		this.fileName = fileName;
		this.notifier = notifier;
	}

	@Override
	public void read() {
		try (Scanner input = new Scanner(new File(fileName))) {
			while (input.hasNext()) {
				notifier.notifySubscribers(input.next());
			}
		} catch (FileNotFoundException e) {
			System.err.println(e);
		}
	}

}
