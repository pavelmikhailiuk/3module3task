package observer.reader;

public interface WordReader {
	void read();
}
